package test

class BootStrap {

    def init = { servletContext ->
        if (!Address.findByName('address 1')) {
            new Address(name: 'address 1').save(failOnError: true)
        }
        if (!Address.findByName('address 2')) {
            new Address(name: 'address 2').save(failOnError: true)
        }
        if (!Address.findByName('address 3')) {
            new Address(name: 'address 3').save(failOnError: true)
        }
    }
    def destroy = {
    }
}
